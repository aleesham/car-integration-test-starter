package org.launchcode.training.controllers;

import org.launchcode.training.data.CarMemoryRepository;
import org.launchcode.training.models.Car;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by LaunchCode
 */
@Controller
@RequestMapping("car")
public class CarController {

    private CarMemoryRepository carRepository = new CarMemoryRepository();

    @RequestMapping(value ="/seedData")
    public String seedDatabase() {
        // Go to /car/seedData in browser to add some cars to your app
        Car prius = new Car("Toyota", "Prius", 10, 50);
        Car jeep = new Car("Jeep", "Grand Cherokee ", 20, 25);
        Car jetta = new Car("Volkswagen", "Jetta", 12, 35);
        carRepository.save(prius);
        carRepository.save(jeep);
        carRepository.save(jetta);
        return "car/seeded";
    }

    @RequestMapping
    public String index(Model model){
        List<Car> cars = carRepository.findAll();
        model.addAttribute("cars", cars);
        return "car/index";
    }

    @RequestMapping(value="/{id}")
    public String viewCarById(Model model, @PathVariable int id){
        Car car = carRepository.findById(id);
        model.addAttribute("car", car);
        return "car/view";
    }

}
