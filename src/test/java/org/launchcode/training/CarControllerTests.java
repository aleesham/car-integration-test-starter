package org.launchcode.training;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.launchcode.training.data.CarMemoryRepository;
import org.launchcode.training.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CarControllerTests {

    @Autowired
    private MockMvc mockMvc;

    private CarMemoryRepository carRepository = new CarMemoryRepository();

    @Test
    public void viewCarById() throws Exception {
        Car car = new Car("Hello", "World", 5, 19);
        carRepository.save(car);
        mockMvc.perform(get("/car/" + car.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void multipleCarsShowOnIndex() throws Exception {
        Car car1 = new Car("Ford", "Mustang", 14, 28.6);
        Car car2 = new Car("Zoom", "Zoom", 1, 1);
        carRepository.save(car1);
        carRepository.save(car2);

        mockMvc.perform(get("/car"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Ford")))
                .andExpect(content().string(containsString("Zoom")));

//        Item item = new Item("Test Item", 5);
//        itemRepository.save(item);
//        mockMvc.perform(post("/car/add-item/")
//                .param("ids", Integer.toString(item.getUid())))
//                .andExpect(status().is3xxRedirection())
//                .andExpect(header().string("Location", "/car"));
//        mockMvc.perform(get("/car/"))
//                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(item.getName())));
    }

}
